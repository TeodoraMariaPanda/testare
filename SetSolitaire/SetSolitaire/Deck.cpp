#include "Deck.h"



Deck::Deck()
{
	for (size_t number = 0; number != static_cast<int>(Card::Number::DEFAULT); number++)
	{
		for (size_t symbol = 0; symbol != static_cast<int>(Card::Symbol::DEFAULT); symbol++)
		{
			for (size_t shading = 0; shading != static_cast<int>(Card::Shading::DEFAULT); shading++)
			{
				for (size_t color = 0; color != static_cast<int>(Card::Color::DEFAULT); color++)
				{
					Card c(static_cast<Card::Number>(number), static_cast<Card::Symbol>(symbol), static_cast<Card::Shading>(shading),
						static_cast<Card::Color>(color));
					m_deck.push_back(c);
				}
			}
		}
	}
	m_cardsLeft = m_deck.size();
}


Deck::~Deck()
{
}

void Deck::Shuffle()
{
	std::random_device rd;
	std::mt19937 gen(rd());
	std::uniform_int_distribution<> dis(0, 51);

	for (auto&& elem : m_deck)
	{
		int randomCardIndex = dis(gen);
		elem.Swap(m_deck.at(randomCardIndex));
	}
}

Card & Deck::DealCard()
{
	if (m_cardsLeft == 0)
	{
		Shuffle();
		m_cardsLeft = m_deck.size();
	}
	else {
		return m_deck[--m_cardsLeft];
	}
}

size_t Deck::GetCardsLeft()
{
	return m_cardsLeft;
}
