#pragma once
#include "Deck.h"
#include "Board.h"
class Dealer
{
	Deck m_deck;
public:
	Dealer(const Deck& deck);
	~Dealer();
	void FirstHand(Board& board);
	bool ReplaceSet(Board& board);
};

