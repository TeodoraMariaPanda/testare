#include "Board.h"



Board::Board()
{
}


Board::~Board()
{
}

void Board::DisplayBoard() const
{
	for (auto card : m_cards)
	{
		card.Show();
	}
}

void Board::AddCardToBoard(const Card & card)
{
	m_cards.push_back(card);
}

bool Board::IsMinimumAchieved() const
{
	return m_cards.size()>=12;
}

bool Board::FindSets()
{
	for (size_t i = 0; i < m_cards.size() - 2; i++)
	{
		for (size_t j = i + 1; j < m_cards.size() - 1; j++)
		{
			for (size_t k = j + 1; k < m_cards.size(); k++)
			{
				if (ValidateSets(i, j, k))
				{
					RemoveSet(i, j, k);
					return true;
				}
			}
		}
	}
	return false;
}

bool Board::ValidateSets(const size_t & poz1, const size_t & poz2, const size_t & poz3) const
{
	return m_cards[poz1].FormsSet(m_cards[poz2], m_cards[poz3]);
}

void Board::RemoveSet(const size_t & poz1, const size_t & poz2, const size_t & poz3)
{
	if (poz3 > poz2 && poz3 > poz1)
	{
		m_cards.erase(m_cards.begin() + poz3);
		if (poz2 > poz1)
		{
			m_cards.erase(m_cards.begin() + poz2);
			m_cards.erase(m_cards.begin() + poz1);
		}
		else {
			m_cards.erase(m_cards.begin() + poz1);
			m_cards.erase(m_cards.begin() + poz2);
		}
	}
	if (poz2 > poz3 && poz2 > poz1)
	{
		m_cards.erase(m_cards.begin() + poz2);
		if (poz3 > poz1)
		{
			m_cards.erase(m_cards.begin() + poz3);
			m_cards.erase(m_cards.begin() + poz1);
		}
		else {
			m_cards.erase(m_cards.begin() + poz1);
			m_cards.erase(m_cards.begin() + poz3);
		}
	}
	if (poz1 > poz3 && poz1 > poz2)
	{
		m_cards.erase(m_cards.begin() + poz1);
		if (poz3 > poz2)
		{
			m_cards.erase(m_cards.begin() + poz3);
			m_cards.erase(m_cards.begin() + poz2);
		}
		else {
			m_cards.erase(m_cards.begin() + poz2);
			m_cards.erase(m_cards.begin() + poz3);
		}
	}
}
