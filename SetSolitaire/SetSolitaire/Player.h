#pragma once
class Player
{
	bool m_wasDealer = false;
	int m_points = 0;
	unsigned int m_setsFound = 0;
public:
	Player();
	~Player();
	void FoundSet();
	void UpdatePoints();
	void LosePoint();
	void BecomeDealer();
	bool WasDealer() const;
	int GetPoints() const;
};

