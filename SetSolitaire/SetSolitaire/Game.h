#pragma once
#include <vector>
#include "Player.h"
#include "Dealer.h"
class Game
{
	std::vector<Player> m_players;
	bool m_isGameOver = false;
public:
	Game(size_t playerNumber = 1);
	~Game();
	void Play(Board& board,Dealer& dealer);
};

