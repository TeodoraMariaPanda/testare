#pragma once
#include <vector>
#include "Card.h"
#include <random>
class Deck
{
	std::vector<Card> m_deck;
	size_t m_cardsLeft;
public:
	Deck();
	~Deck();
	void Shuffle();
	Card& DealCard();
	size_t GetCardsLeft();
};

