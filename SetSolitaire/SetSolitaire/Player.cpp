#include "Player.h"



Player::Player()
{
}


Player::~Player()
{
}

void Player::FoundSet()
{
	m_setsFound++;
}

void Player::UpdatePoints()
{
	m_points += m_setsFound;
}

void Player::LosePoint()
{
	m_points--;
}

void Player::BecomeDealer()
{
	m_wasDealer = true;
}

bool Player::WasDealer() const
{
	return m_wasDealer;
}

int Player::GetPoints() const
{
	return m_points;
}
