#include "Game.h"
#include "Dealer.h"
#include "Board.h"



Game::Game(size_t playerNumber)
{
	for (size_t index = 0; index < playerNumber; index++)
	{
		m_players.push_back(Player());
	}
}


Game::~Game()
{
}

void Game::Play(Board & board, Dealer & dealer)
{
	for (size_t turn = 0; turn < m_players.size(); turn++)
	{
		m_players[turn].BecomeDealer();
		m_isGameOver = false;
		dealer.FirstHand(board);
		while (!m_isGameOver)
		{
			size_t playerIndex;
			std::cout << "Which player calls set?" << std::endl;
			std::cin >> playerIndex;
			if (playerIndex >= 0)
			{
				if (board.FindSets())
				{
					m_players[playerIndex].FoundSet();
				}
				else {
					m_players[playerIndex].LosePoint();
				}
			}
			if (!dealer.ReplaceSet(board))
			{
				m_isGameOver = true;
			}
		}
		for (auto player: m_players)
		{
			player.UpdatePoints();
		}
	}
}
