#pragma once
#include "Card.h"
#include <vector>
class Board
{
	std::vector<Card> m_cards;
public:
	Board();
	~Board();
	void DisplayBoard() const;
	void AddCardToBoard(const Card& card);
	bool IsMinimumAchieved() const;
	bool FindSets();
	bool ValidateSets(const size_t& poz1,const size_t& poz2, const size_t& poz3) const;
	void RemoveSet(const size_t& poz1, const size_t& poz2, const size_t& poz3);
};

