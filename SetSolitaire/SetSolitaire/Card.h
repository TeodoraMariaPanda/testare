#pragma once
#include <iostream>
#include <string>
class Card
{
public:
	enum class Number {ONE, TWO, THREE, DEFAULT};
	enum class Symbol {DIAMOND, SQUIGGLE, OVAL, DEFAULT};
	enum class Shading {SOLID, STRIPED, OPEN, DEFAULT};
	enum class Color {RED, GREEN, BLUE, DEFAULT};

private:
	Number m_number;
	Symbol m_symbol;
	Shading m_shading;
	Color m_color;

	static const std::string* numberValuePicker;
	static const std::string* symbolValuePicker;
	static const std::string* shadingValuePicker;
	static const std::string* colorValuePicker;

	bool IsCardStateDefined() const;

public:
	Card(const Number& number = Number::DEFAULT, const Symbol& symbol = Symbol::DEFAULT, const Shading& shading = Shading::DEFAULT, 
		const Color&color = Color::DEFAULT);
	Card(const Card& card);
	~Card();
	Card& operator = (const Card& card);
	void Show() const;
	void Swap(Card &secondCard);
	bool FormsSet(const Card& first, const Card& second)const;
};
