#include "Dealer.h"



Dealer::Dealer(const Deck& deck)
{
	m_deck = deck;
	m_deck.Shuffle();
}


Dealer::~Dealer()
{
}

void Dealer::FirstHand(Board & board)
{
	for (size_t nr = 0; nr < 12; nr++)
	{
		board.AddCardToBoard(m_deck.DealCard());
	}
	board.DisplayBoard();
}

bool Dealer::ReplaceSet(Board & board)
{
	if (m_deck.GetCardsLeft()>3)
	{
		for (size_t nr = 0; nr < 3; nr++)
		{
			board.AddCardToBoard(m_deck.DealCard());
		}
		return true;
	}
	else {
		while (m_deck.GetCardsLeft() > 0)
		{
			board.AddCardToBoard(m_deck.DealCard());
		}
		return false;
	}
}
