#include "Card.h"

static const std::string numberStrings[] = { "One", "Two", "Three" };
const std::string* Card::numberValuePicker = numberStrings;
static const std::string symbolStrings[] = { "Diamond", "Squiggle", "Oval" };
const std::string* Card::symbolValuePicker = symbolStrings;
static const std::string colorStrings[] = { "Red", "Green", "Blue" };
const std::string* Card::colorValuePicker = colorStrings;
static const std::string shadingStrings[] = { "Solid", "Striped", "Open" };
const std::string* Card::shadingValuePicker = shadingStrings;

bool Card::IsCardStateDefined() const
{
	if (m_number == Number::DEFAULT || m_symbol == Symbol::DEFAULT || m_shading == Shading::DEFAULT || m_color == Color::DEFAULT)
	{
		return false;
	}
	return true;
}

Card::Card(const Number & number, const Symbol & symbol, const Shading & shading, const Color & color):m_number(number), m_symbol(symbol), m_shading(shading), m_color(color)
{
}

Card::Card(const Card & card)
{
	*this = card;
}

Card::~Card()
{
}

Card & Card::operator=(const Card & card)
{
	m_number = card.m_number;
	m_symbol = card.m_symbol;
	m_shading = card.m_shading;
	m_color = card.m_color;

	return *this;
}

void Card::Show() const
{
	if (IsCardStateDefined())
	{
		std::cout << numberValuePicker[static_cast<int>(m_number)] << " " << symbolValuePicker[static_cast<int>(m_symbol)]
			<< " " << shadingValuePicker[static_cast<int>(m_shading)] << " " << colorValuePicker[static_cast<int>(m_color)] << std::endl;
	}
	else
	{
	std::cout << "Card is undefined" << std::endl;
	}
}

void Card::Swap(Card &secondCard)
{
	Card temp = *this;
	*this = secondCard;
	secondCard = temp;
}

bool Card::FormsSet(const Card & first, const Card & second) const
{
	if ((first.m_number == second.m_number &&  second.m_number == m_number) || (first.m_number != second.m_number &&  second.m_number != m_number && first.m_number != m_number))
	{
		if ((first.m_symbol == second.m_symbol &&  second.m_symbol == m_symbol) || (first.m_symbol != second.m_symbol &&  second.m_symbol != m_symbol && first.m_symbol != m_symbol))
		{
			if ((first.m_shading == second.m_shading &&  second.m_shading == m_shading) || (first.m_shading != second.m_shading &&  second.m_shading != m_shading && first.m_shading != m_shading))
			{
				if ((first.m_color == second.m_color &&  second.m_color == m_color) || (first.m_color != second.m_color &&  second.m_color != m_color && first.m_color != m_color))
				{
					return true;
				}
			}
		}
	}
	return false;
}
